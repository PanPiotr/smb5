package pl.edu.pja.smb5;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

public class AudioService extends Service {

    private boolean isPlaying = false;
    private MediaPlayer mediaPlayer;

    public AudioService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(this, R.raw.canon);
        }
        if (isPlaying) {
            pausePlaying();
        } else {
            startPlaying();
        }
        return START_NOT_STICKY;
    }

    private void startPlaying() {
        isPlaying = true;
        mediaPlayer.start();
    }

    private void pausePlaying() {
        isPlaying = false;
        mediaPlayer.pause();
    }

    @Override
    public void onDestroy() {
        mediaPlayer.release();
        mediaPlayer = null;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
