package pl.edu.pja.smb5;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.RemoteViews;

/**
 * Implementation of App Widget functionality.
 */
public class SmbAppWidget extends AppWidgetProvider {

    public static final String ACTION_TOGGLE_IMAGE = "ACTION_TOGGLE_IMAGE";
    public static final String ACTION_TOGGLE_MUSIC = "ACTION_TOGGLE_MUSIC";
    private static boolean isImageShown = false;
    private static boolean isPlaying = false;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);

        //setting URL opening
        Intent webpageIntent = new Intent(Intent.ACTION_VIEW);
        webpageIntent.setData(Uri.parse("http://www.wykop.pl"));
        PendingIntent pi = PendingIntent.getActivity(context, 101, webpageIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.open_webpage, pi);

        //setting image showing
        Intent imageIntent = new Intent(context, SmbAppWidget.class);
        imageIntent.setAction(ACTION_TOGGLE_IMAGE);
        PendingIntent imagePendingIntent = PendingIntent.getBroadcast(context, 103, imageIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.toggle_image, imagePendingIntent);

        if (isImageShown) {
            views.setViewVisibility(R.id.example_image, View.VISIBLE);
        } else {
            views.setViewVisibility(R.id.example_image, View.GONE);
        }


        //setting audio player
        Intent playStopIntent = new Intent(context, SmbAppWidget.class);
        playStopIntent.setAction(ACTION_TOGGLE_MUSIC);
        PendingIntent audioPendingIntent = PendingIntent.getBroadcast(context, 102, playStopIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.play_music, audioPendingIntent);

        if (isPlaying) {
            views.setTextViewText(R.id.play_music, context.getString(R.string.pause));
        } else {
            views.setTextViewText(R.id.play_music, context.getString(R.string.play));
        }

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        RemoteViews remoteViews;
        ComponentName smbWidget;

        remoteViews = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
        smbWidget = new ComponentName(context, SmbAppWidget.class);
        if (intent.getAction().equals(ACTION_TOGGLE_IMAGE)) {
            isImageShown = !isImageShown;
            if (isImageShown) {
                remoteViews.setViewVisibility(R.id.example_image, View.VISIBLE);
            } else {
                remoteViews.setViewVisibility(R.id.example_image, View.GONE);
            }

        } else if (intent.getAction().equals(ACTION_TOGGLE_MUSIC)) {
            isPlaying = !isPlaying;
            if (isPlaying) {
                remoteViews.setTextViewText(R.id.play_music, context.getString(R.string.pause));
            } else {
                remoteViews.setTextViewText(R.id.play_music, context.getString(R.string.play));
            }
            Intent playStopIntent = new Intent(context, AudioService.class);
            context.startService(playStopIntent);

        }
        appWidgetManager.updateAppWidget(smbWidget, remoteViews);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

